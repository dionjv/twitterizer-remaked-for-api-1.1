﻿//Not made

//Author:szr.ktkr
//License:BSD License
//using System;
//using Twitterizer;
//using System.Net;
//using Newtonsoft.Json;
//namespace Twitterizer.Methods.Image
//{
//	/// <summary>
//	/// Description of UploadTwitpic.
//	/// </summary>
//	public class UploadTwitpic
//	{
//		/// <summary>
//		/// ImageUpload with twitpic.
//		/// </summary>
//		/// <param name="tokens">set OauthTokens.</param>
//		/// <param name="apikey">set twitpic apikey.</param>
//		/// <param name="msg">set message (not required)</param>
//		/// <param name="ImageFilePath">set Path of imagefile.</param>
//		public UploadTwitpicResults UploadingTwitpic(OAuthTokens tokens, string apikey, string msg, string ImageFilePath)
//		{
//			//API2に対応する時間がないからAPI1
//			//parameter
//			//key (Required): Your API Key.
//			//consumer_token (Required): Your Twitter Consumer Token.
//			//consumer_secret (Required): Your Twitter Consumer Secret.
//			//oauth_token (Required): The Twitter OAuth Token for the user.
//			//oauth_secret (Required): The Twitter OAuth Secret for the user.
//			//message (Required): The tweet that belongs to the image. Can be empty string.
//			//media (Required): The file upload data.
//			string uploaduri = "http://api.twitpic.com/1/upload.json";
//			//Create WebClient
//			WebClient wc = new WebClient();
//			//Create NameValueCollection
//			System.Collections.Specialized.NameValueCollection ps =
//    new System.Collections.Specialized.NameValueCollection();
//			ps.Add("key",apikey);
//			ps.Add("consumer_token",tokens.ConsumerKey);
//			ps.Add("consumer_secret",tokens.ConsumerSecret);
//			ps.Add("oauth_token",tokens.AccessToken);
//			ps.Add("oauth_secret",tokens.AccessTokenSecret);
//			ps.Add("message",msg);
//			ps.Add("media",ImageFilePath);
//			byte[] resData = wc.UploadValues(uploaduri, ps);
//			wc.Dispose();
//			string resText = System.Text.Encoding.UTF8.GetString(resData);
//			UploadTwitpicResults results = JsonConvert.DeserializeObject<UploadTwitpicResults>(resText);
//		return results;
//		}
//	}
//	
//	public class UploadTwitpicResults{
//		string id;
//		string text;
//		string url;
//		string width;
//		string height;
//		string type;
//		UploadTwitpicuserResults user;
//	}
//	public class UploadTwitpicuserResults{
//		string id;
//		string screen_name;
//	}
//}
